import logo from './logo.svg';
import './App.css';

function App() {
  //here is an exmple how to create an array
  const dates = ["Monday", "Tuesday", "Wednesday","Thursday","Friday","Saturday","Sunday"];
  //here is an example how to use the array above to create an array of option elements
  //see https://www.w3schools.com/jsref/jsref_map.asp
  const uldates = dates.map((date, index) => {
return <li key={index}>{date}</li>;
});


  return (
    <div className="App">
      <h2>Weekday</h2>
  <tr>{uldates}</tr>
    </div>
  );
}

export default App;
